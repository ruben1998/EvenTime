package eventime.com.eventtime.Objetos;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.List;
import eventime.com.eventtime.R;

public class Adapter extends RecyclerView.Adapter<Adapter.EventosViewHolder>{
    private StorageReference mStorage;
    private List<Eventos> eventos;
    private Context context;

    public Adapter(List<Eventos> eventos) {
        this.eventos = eventos;
    }


    //Holder de REcyclerview
    @Override
    public EventosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View  v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recycler, parent, false);
        EventosViewHolder holder = new EventosViewHolder(v);
        context = parent.getContext();
        return holder;
    }

    @Override
    public void onBindViewHolder(EventosViewHolder holder, int position) {
        mStorage = FirebaseStorage.getInstance().getReference("fotos"); //referencia al storage de firebase
        Eventos post = eventos.get(position); //cursor que recorre los valores de la tabla eventos
        String fotoEvento = post.getImageUrl(); //para conseguir la url
        Glide.with(context).load(fotoEvento).fitCenter().centerCrop().into(holder.ivFotoEvento); //muestra foto mediante url
        holder.tvDescripcion.setText("Descripcion: " + post.getDescripcion()); //muesstra dato que hay en la llave descripcion de base de datos firebase
        holder.tvAutor.setText("Publicado por: "+ post.getAutor()); //muestra usuario que publico
        holder.tvLugar.setText("Lugar: " + post.getLugar()); //muestra lugar
        holder.tvFechaHora.setText("Fecha y hora: " + post.getFechaHora()); //muestra la fecha y hora

    }

    @Override
    public int getItemCount() {
        return eventos.size();
    }

    public static class EventosViewHolder extends RecyclerView.ViewHolder{
        ImageView ivFotoEvento;
        TextView tvLugar, tvFechaHora, tvAutor, tvDescripcion;
        public EventosViewHolder(View itemView) {
            super(itemView);
            tvDescripcion = (TextView) itemView.findViewById(R.id.tvDescripcion);
            ivFotoEvento = (ImageView) itemView.findViewById(R.id.ivEvento);
            tvLugar = (TextView) itemView.findViewById(R.id.tvLugarEvento);
            tvFechaHora = (TextView) itemView.findViewById(R.id.tvFechaHoraEvento);
            tvAutor = (TextView) itemView.findViewById(R.id.tvAutorEvento);

        }
    }
}
