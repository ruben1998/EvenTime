package eventime.com.eventtime;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class creargrupo extends AppCompatActivity {
Button creargrupo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creargrupo);
        creargrupo = (Button) findViewById(R.id.btnCreargrupo);
    }
    public void Creargrupo(View view){
        Toast.makeText(this, "Grupo creado exitosamente", Toast.LENGTH_SHORT).show();
        Intent creargrupo = new Intent(this, principal.class);
        startActivity(creargrupo);
    }
}
