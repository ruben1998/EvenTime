package eventime.com.eventtime;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import eventime.com.eventtime.Objetos.Referencia;
import eventime.com.eventtime.Objetos.Usuario;


public class usuario extends AppCompatActivity {
    Button retroceder;
    TextView txtNombre, txtApellidos, txtEmail, txtGrupo;

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);
        retroceder = (Button) findViewById(R.id.btnRegresar);
        txtNombre = (TextView) findViewById(R.id.evnombre);
        txtApellidos = (TextView) findViewById(R.id.evapellidos);
        txtEmail = (TextView) findViewById(R.id.eveEmail);
        mAuth = FirebaseAuth.getInstance();
        final String emailFirebase = mAuth.getCurrentUser().getDisplayName();

        FirebaseDatabase evenTime = FirebaseDatabase.getInstance();
        final DatabaseReference reference = evenTime.getReference(Referencia.REFERENCIA_PRINCIPAL);

        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = mAuth.getCurrentUser();
                if (user != null){
                    final String emailFirebase = mAuth.getCurrentUser().getDisplayName()
                            ;
                }
            }
        };
/*
        final DatabaseReference referencia = evenTime.getReference(Referencia.REFERENCIA_USUARIO);
        Usuario usuario = new Usuario(email);
        Query q = referencia.orderByChild(emailFirebase).equalTo(email);
        q.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
    }
    public void Retroceder(View view){
        Toast.makeText(this, "Regresando a pantalla principal", Toast.LENGTH_SHORT).show();
        Intent retroceder = new Intent(this, principal.class);
        startActivity(retroceder);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(listener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(listener);
    }

}
